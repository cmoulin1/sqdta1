*** Settings ***
Documentation       Un premier test d'accès à la page de login.
Library    SeleniumLibrary

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com
${browser}    firefox

*** Test Cases ***
Login page test case

    # Ouverture d'un navigateur Firefox à la page CuraHealthcareService #
    Open Browser    ${url}    ${browser}

    # Maximiser la fenêtre active #
    Maximize Browser Window

    # Cliquer sur le bouton MakeAppointment #
    Click Element    id:btn-make-appointment

    # Renseigner le champ Username #
    Input Text    id:txt-username    John Doe

    # Renseigner le champ Password #
    Input Text    id:txt-password    ThisIsNotAPassword

    # Cliquer sur le bouton de connexion #
    Click Button    id:btn-login

    # Pause de 5 secondes pour constater la bonne connexion #
    Sleep    5

    # Fermeture du navigateur #
    Close Browser