*** Settings ***
Resource	 squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_9_SETUP}	Get Variable Value	${TEST 9 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_9_SETUP is not None	${__TEST_9_SETUP}

Test Teardown
	${__TEST_9_TEARDOWN}	Get Variable Value	${TEST 9 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_9_TEARDOWN is not None	${__TEST_9_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Cas de test BDD
	[Setup]	   Test Setup

	Given je suis sur la page CuraLogin
	When Je rentre un ${login}
	And je rentre un ${mdp}
	And je clique sur OK
	Then Je peux faire un appointment

	[Teardown]	  Test Teardown